import pandas as pd
import matplotlib.pyplot as plt
import os


fhead='Osciloscope'
files=os.listdir()
cfiles=[]
for file in files:
    if fhead in file:
        cfiles.append(file)

dfi=[]
df0=pd.DataFrame()
for file in cfiles:
    df0=pd.read_excel(file)
    if 'Osciloscope Current (pA)'in df0.columns:
        try:
            #ax=df0.plot('Time','Osciloscope Current (pA)', title=file)
            #ax.figure.autofmt_xdate()
            #ax.set_ylabel("I(pA)")
            # plt.show()
            dfi.append(df0)
        except TypeError:
            print('Borrar: '+file)
            pass

#timestamp definintion

fhead='Timestamp'
files=os.listdir()
cfiles=[]
for file in files:
    if fhead in file:
        cfiles.append(file)

tsi=[]
ts0=pd.DataFrame()
for file in cfiles:
    ts0=pd.read_excel(file)
    ts0['TT']=ts0[ts0.columns[1]]+ts0[ts0.columns[2]]*1.0e-9
    ts0['Z']=0
    print(file)
    try:
        # ax=ts0.plot('Time', 'TT', style='.', title=file)
        # ax.figure.autofmt_xdate()
        # ax.set_ylabel("Ts (s)")
        # plt.show()
        tsi.append(ts0)
    except TypeError:
        print('Borrar: '+file)
        pass

##Label function
def label_point(x, y, val, ax, label=''):
    a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
    for i, point in a.iterrows():
        #ax.text(point['x'], point['y'], str(point['val']), ha='left', rotation=45)
        ax.annotate(label+str(point['val']), (point['x'],point['y']), xytext=(10,-5),
                    textcoords='offset points', family='sans-serif', color='darkslategrey')

##plot osciloscope data

df=pd.concat(dfi)
ts=pd.concat(tsi)
df.dropna(how="all", inplace=True)
ts.dropna(how="all", inplace=True)
tp=ts.plot('Time', 'Z', style='o', label='Timestamp')
label_point(ts.Time, ts.Z, ts.Time, tp)
label_point(ts.Time, ts.Z-500, ts.TT, tp, 'Relative Timestamp*: ')
label_point(ts.Time, ts.Z-1000, ts.Temperature, tp, "Temp(c): ")
label_point(ts.Time, ts.Z-1500, ts.Pressure, tp, "Press(hPa): ")
ax=df.plot('Time', 'Osciloscope Current (pA)', ax=tp)

ax.figure.autofmt_xdate()
ax.set_ylabel("I(pA)")
# ax=df.plot('Time', 'Temperature')
# ax.figure.autofmt_xdate()
# ax.set_ylabel("C")
# ax=df.plot('Time', 'Pressure')
# ax.figure.autofmt_xdate()
# ax.set_ylabel("hPa")
# ax=df.plot('Time')
plt.show()
