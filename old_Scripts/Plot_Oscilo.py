import pandas as pd
import matplotlib.pyplot as plt
import os


fhead='Osciloscope'
files=os.listdir()
cfiles=[]
for file in files:
    if fhead in file:
        cfiles.append(file)

dfi=[]
df0=pd.DataFrame()
for file in cfiles:
    df0=pd.read_excel(file)
    if 'Osciloscope Current (pA)'in df0.columns:
        try:
            #ax=df0.plot('Time','Osciloscope Current (pA)', title=file)
            #ax.figure.autofmt_xdate()
            #ax.set_ylabel("I(pA)")
            # plt.show()
            dfi.append(df0)
        except TypeError:
            print('Borrar: '+file)
            pass

df=pd.concat(dfi)
df.dropna(how="all", inplace=True)
ax=df.plot('Time', 'Osciloscope Current (pA)')
ax.figure.autofmt_xdate()
ax.set_ylabel("I(pA)")
ax=df.plot('Time', 'Temperature')
ax.figure.autofmt_xdate()
ax.set_ylabel("C")
ax=df.plot('Time', 'Pressure')
ax.figure.autofmt_xdate()
ax.set_ylabel("hPa")
ax=df.plot('Time')
plt.show()
