#!/usr/bin/python3.6

import pandas as pd
import matplotlib.pyplot as plt
import os


fhead='Timestamp'
files=os.listdir()
cfiles=[]
for file in files:
    if fhead in file:
        cfiles.append(file)

dfi=[]
df0=pd.DataFrame()
for file in cfiles:
    df0=pd.read_excel(file)
    df0['TT']=df0[df0.columns[1]]+df0[df0.columns[2]]*1.0e-9
    print(file)
    try:
        ax=df0.plot('Time', 'TT', style='.', title=file)
        ax.figure.autofmt_xdate()
        ax.set_ylabel("Ts (s)")
        plt.show()
        dfi.append(df0)
    except TypeError:
        print('Borrar: '+file)
        pass

# df=pd.concat(dfi)
# df.dropna(how="all", inplace=True)
# ax=df.plot('Time', 'Osciloscope Current (pA)')
# ax.figure.autofmt_xdate()
# ax.set_ylabel("I(pA)")
# ax=df.plot('Time', 'Temperature')
# ax.figure.autofmt_xdate()
# ax.set_ylabel("C")
# ax=df.plot('Time', 'Pressure')
# ax.figure.autofmt_xdate()
# ax.set_ylabel("hPa")
# plt.show()
