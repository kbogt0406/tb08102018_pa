import pandas as pd
import matplotlib.pyplot as plt
import os


fhead='Osciloscope'
files=os.listdir()
cfiles=[]
for file in files:
    if fhead in file:
        cfiles.append(file)

dfi=[]
df0=pd.DataFrame()
for file in cfiles:
    df0=pd.read_excel(file)
    if 'Osciloscope Current (pA)'in df0.columns:
        try:
            ax=df0.plot('Time','Osciloscope Current (pA)', title=file)
            ax.figure.autofmt_xdate()
            ax.set_ylabel("I(pA)")
            #plt.show()
            dfi.append(df0)
        except TypeError:
            pass
#df= pd.read_excel("Osciloscope_18-10-28_2241_001.xlsx")
df=pd.concat(dfi)
ax=df.plot('Time')
ax.figure.autofmt_xdate()
plt.show()
